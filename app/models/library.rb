class Library < ActiveRecord::Base
	has_many :reservations, dependent: :destroy
	has_many :rooms, dependent: :destroy
end
