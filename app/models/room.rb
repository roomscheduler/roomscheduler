class Room < ActiveRecord::Base
  belongs_to :library
  has_many :reservations, dependent: :destroy
end
