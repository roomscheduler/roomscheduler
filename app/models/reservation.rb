class Reservation < ActiveRecord::Base
  belongs_to :room
  belongs_to :library
  belongs_to :user
end
