class UserMailer < ApplicationMailer
	default from: ENV['SMTP_USERNAME']

	def sample_email(user)
    @user = user
    mail(to: @user.email, subject: 'User Created')
  end
end
