json.array!(@rooms) do |room|
  json.extract! room, :id, :library_id, :seating_capacity, :amenities, :roomtype
  json.url room_url(room, format: :json)
end
