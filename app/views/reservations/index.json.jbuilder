json.array!(@reservations) do |reservation|
  json.extract! reservation, :id, :booking_id, :room_id, :library_id, :user_id, :start_time, :end_time, :title, :description
  json.url reservation_url(reservation, format: :json)
end
