class SendTextController < ApplicationController
  def index
  end

  def send_text_message
    number_to_send_to = params[:number_to_send_to]

    twilio_sid = "ACc629aae0d6938192ad6ec3d049a31504"
    twilio_token = "37fbc61fab94fc1a6244b7ca3b859a13"
    twilio_phone_number = "2184607910"

    @twilio_client = Twilio::REST::Client.new twilio_sid, twilio_token

    @twilio_client.account.sms.messages.create(
      :from => "+1#{twilio_phone_number}",
      :to => number_to_send_to,
      :body => "Your  reservation is confirmed ! please bring your id card to the library"
    )
  end
end