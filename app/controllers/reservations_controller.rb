class ReservationsController < ApplicationController
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]

  # GET /reservations
  # GET /reservations.json
  def index
  @reservations = Reservation.all
  @date = Date.today
	setlibrary
  setlist
 
 
end

  # GET /reservations/1
  # GET /reservations/1.json
  def show
  end

  def update_table

  @reservations = Reservation.all
  setlibrary
  
  #@date = Date.parse(params[:selected_date] )
  @date =  DateTime.strptime( params[:selected_date], '%m/%d/%Y')
  setlist
  #binding.pry
  respond_to do |format|
    format.js
    end
  end

  # GET /reservations/new
  def new
    @reservation = Reservation.new
  end

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  # POST /reservations.json
  def create
    @reservation = Reservation.new(reservation_params)
    @reservation.user_id = current_user.id
    send_text_message( @reservation.phone , @reservation.start_time , @reservation.end_time)
    

    respond_to do |format|
      if @reservation.save
        format.html { redirect_to @reservation, notice: I18n.t('views.reservations.resmsg1') }

        format.json { render :show, status: :created, location: @reservation }
      else
        format.html { render :new }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end

  end

  # PATCH/PUT /reservations/1
  # PATCH/PUT /reservations/1.json
  def update
    respond_to do |format|
      if @reservation.update(reservation_params)
        format.html { redirect_to @reservation, notice: I18n.t('views.reservations.resmsg2') }
        format.json { render :show, status: :ok, location: @reservation }
      else
        format.html { render :edit }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end


  def send_text_message(phonenumber, start_time, end_time )
    number_to_send_to = phonenumber    #params[:number_to_send_to]
    starttime = start_time.to_s
    endtime  =  end_time.to_s
    twilio_sid = "ACc629aae0d6938192ad6ec3d049a31504"
    twilio_token = "37fbc61fab94fc1a6244b7ca3b859a13"
    twilio_phone_number = "2184607910"

    @twilio_client = Twilio::REST::Client.new twilio_sid, twilio_token

    @twilio_client.account.sms.messages.create(
      :from => "+1#{twilio_phone_number}",
      :to => number_to_send_to,
      :body => "Your  reservation from " + starttime + " to " + endtime + " is confirmed !"
    )
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy
    @reservation.destroy
    respond_to do |format|
      format.html { redirect_to reservations_url, notice: I18n.t('views.reservations.resmsg3') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_params
      params.require(:reservation).permit(:booking_id, :room_id, :library_id, :user_id, :start_time, :end_time , :phone ,:title, :description)
    end
    def setlibrary
      if params[:id].present?
      @library = Library.find(params[:id])
      else
      @library = Library.find(1)
      end     
    end

    def setlist
     
      @list = []
    for i in 0..6
      for k in @library.rooms   
         for j in 8..19 
        @availableRooms = Reservation.select(%q{count(id) as count2}).where("start_time <= ?", DateTime.new((@date+i.days).strftime('%Y').
          to_i, (@date+i.days).strftime('%m').to_i, (@date+i.days).strftime('%d').to_i, j, 0, 0)).where("end_time > ?", DateTime.new((@date+i.days).strftime('%Y').
          to_i, (@date+i.days).strftime('%m').to_i, (@date+i.days).strftime('%d').to_i, j, 0, 0)).where(:room_id => k.id)
          @list << @availableRooms[0]
          end
      end
    end
    
    end
end
