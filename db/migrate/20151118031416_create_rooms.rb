class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.references :library, index: true, foreign_key: true
      t.integer :seating_capacity
      t.text :amenities
      t.string :roomtype

      t.timestamps null: false
    end
  end
end
