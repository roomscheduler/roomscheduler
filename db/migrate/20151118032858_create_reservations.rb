class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :booking_id
      t.references :room, index: true, foreign_key: true
      t.references :library, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.datetime :start_time
      t.datetime :end_time
      t.string :title
      t.text :description

      t.timestamps null: false
    end
  end
end
