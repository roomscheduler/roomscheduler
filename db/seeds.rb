# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first) 
#users = users.create([{email: "bharadwaj.vdtn@gmail.com"},{encrypted_password: "$2a$10$jss3P1chkmJrTlRNsg6Sg.JWgso86qMRd/A1VaVN2j3VeII1hTHVK"},{role: "admin"}])
libraries = Library.create([{lib_name: "zimmerman Library"}]) 
rooms = Room.create( library_id: "1" , seating_capacity: "6", amenities: "monitor, projector", roomtype: "student" , created_at: "",updated_at: "" )
rooms = Room.create( library_id: "1" , seating_capacity: "4", amenities: "monitor, white board", roomtype: "student" , created_at: "",updated_at: "" )
rooms = Room.create( library_id: "1" , seating_capacity: "10", amenities: "coffee machine, projector", roomtype: "student" , created_at: "",updated_at: "" )